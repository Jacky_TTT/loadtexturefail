module.exports = {
  resolver: {
    assetExts: [
      "ttf",
      "mp4",
      "otf",
      "dae",
      "obj",
      "mtl",
      "amf",
      "3mf",
      "3ds",
      "jpg",
      "assimp",
      "fbx",
      "pmd",
      "vmd",
      "ply",
      "stl",
      "vtk",
      "vtp",
      "babylon",
      "drc",
      "pcd",
      "pack",
      "bvh",
      "x",
      "png"
    ]
  }
}