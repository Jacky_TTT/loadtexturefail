import { View as GraphicsView } from 'expo-graphics';
import ExpoTHREE, { THREE } from 'expo-three';
import React from 'react';
import { Platform } from 'react-native';
import Assets from './Assets';
import TouchableView from './TouchableView';
import 'three/examples/jsm/controls/OrbitControls.js';

export default class App extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      selectedIndex: 0,
    };

    this.planeObjects = []
    this.raycaster = new THREE.Raycaster();
    this.prevDiceTexture = ''
  }

  componentDidMount() {
    // Turn off extra warnings
    THREE.suppressExpoWarnings(true)
  }

  onShouldReloadContext = () => {
    /// The Android OS loses gl context on background, so we should reload it.
    return Platform.OS === 'android';
  };

  render() {
    if (this.controls)
      this.controls.enableRotate = this.props.allowRotate;

    // Create an `ExpoGraphics.View` covering the whole screen, tell it to call our
    // `onContextCreate` function once it's initialized.
    return (
      <TouchableView style={{ flex: 1 }} shouldCancelWhenOutside={false}>
        <GraphicsView
          style={{ flex: 1 }}
          onContextCreate={this.onContextCreate}
          onRender={this.onRender}
          onResize={this.onResize}
          onShouldReloadContext={this.onShouldReloadContext}
        />
      </TouchableView>
    );
  }

  componentDidUpdate(prevProps) {
    const { prevDiceIndex2 = 0 } = prevProps
    const { diceTexture } = this.props
    if (this.prevDiceTexture !== diceTexture) {
      // console.log("diceIndex")
      // console.log(this.prevDiceIndex)
      // console.log(diceIndex)
      this.prevDiceTexture = diceTexture
      const model = Assets.models.obj.dice;
      if (this.cubeMaterial) {
        ExpoTHREE.loadAsync(model[diceTexture])
          .then((texture) => {
            this.cubeMaterial.map = texture
          })
      }
    }
  }

  // This is called by the `ExpoGraphics.View` once it's initialized
  onContextCreate = async ({
    gl,
    canvas,
    width,
    height,
    scale: pixelRatio,
  }) => {
    this.renderer = new ExpoTHREE.Renderer({
      gl,
      canvas,
      width,
      height,
      pixelRatio,
      alpha: true,
    });
    this.scene = new THREE.Scene();
    // this.scene.background = new THREE.Color(1, 1, 1);
    this.camera = new THREE.PerspectiveCamera(75, width / height, 0.1, 1000);
    this.camera.position.z = this.props.cameraDist ? this.props.cameraDist : 5;
    // const diceIndex = 0;
    // const geometry = new THREE.BoxGeometry(1, 1, 1);
    // const tCubeMaterials = []
    // for (const i = 0; i < 6; i++) {
    //   const textureFile = `dice_${diceIndex}_${i}`;
    //   tCubeMaterials.push(new THREE.MeshBasicMaterial({
    //     map: await ExpoTHREE.loadAsync(Assets.textImages[textureFile])
    //   }));
    // }
    // this.cube = new THREE.Mesh(geometry, tCubeMaterials);
    // this.scene.add(this.cube);


    // // ninja obj model
    // const model = Assets.models.obj.ninja;
    // const object = await ExpoTHREE.loadObjAsync({
    //   asset: model['ninjaHead_Low.obj'],
    // });
    // cubeMaterial = new THREE.MeshBasicMaterial({
    //   map: await ExpoTHREE.loadAsync(model['ao.jpg'])
    // })
    // const geometry = object.children[0].geometry;
    // // geometry.attributes.uv2 = geometry.attributes.uv;
    // // geometry.center();
    // const mesh = new THREE.Mesh(geometry, cubeMaterial);
    // // mesh.scale.multiplyScalar(0.25);
    // ExpoTHREE.utils.scaleLongestSideToSize(mesh, 1);
    // ExpoTHREE.utils.alignMesh(mesh, { y: 1 });
    // this.scene.add(mesh);

    // dice obj model
    const model = Assets.models.obj.dice;
    const object = await ExpoTHREE.loadObjAsync({
      asset: model['polycube.obj'],
    });
    const { diceTexture } = this.props
    this.cubeMaterial = new THREE.MeshBasicMaterial({
      map: await ExpoTHREE.loadAsync(model[diceTexture])
    })
    const geometry = object.children[0].geometry;
    // geometry.attributes.uv2 = geometry.attributes.uv;
    // geometry.center();
    const mesh = new THREE.Mesh(geometry, this.cubeMaterial);
    // mesh.scale.multiplyScalar(0.1);
    ExpoTHREE.utils.scaleLongestSideToSize(mesh, 1);
    // ExpoTHREE.utils.alignMesh(mesh, { y: 1 });
    this.scene.add(mesh);


    // // dummy cube
    // var cgeometry = new THREE.BoxGeometry(1, 1, 1);
    // var cmaterial = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
    // var ccube = new THREE.Mesh(cgeometry, cmaterial);
    // this.scene.add(ccube);
    // console.log(ccube)


    var z_pgeometry = new THREE.PlaneGeometry(1, 1);
    var z_pmaterial = new THREE.MeshLambertMaterial({ color: 0x000000, transparent: true, opacity: 0 });
    var z_pplane = new THREE.Mesh(z_pgeometry, z_pmaterial);
    z_pplane.position.z = 0.5
    this.scene.add(z_pplane);
    this.planeObjects.push(z_pplane)

    var z_2_pgeometry = new THREE.PlaneGeometry(1, 1);
    var z_2_pmaterial = new THREE.MeshLambertMaterial({ color: 0x000000, transparent: true, opacity: 0 });
    var z_2_pplane = new THREE.Mesh(z_2_pgeometry, z_2_pmaterial);
    z_2_pplane.rotation.y = Math.PI
    z_2_pplane.position.z = -0.5
    this.scene.add(z_2_pplane);
    this.planeObjects.push(z_2_pplane)

    var y_pgeometry = new THREE.PlaneGeometry(1, 1);
    var y_pmaterial = new THREE.MeshLambertMaterial({ color: 0x000000, transparent: true, opacity: 0 });
    var y_pplane = new THREE.Mesh(y_pgeometry, y_pmaterial);
    y_pplane.rotation.y = -Math.PI / 2
    y_pplane.position.x = 0.5
    this.scene.add(y_pplane);
    this.planeObjects.push(y_pplane)

    var y_2_pgeometry = new THREE.PlaneGeometry(1, 1);
    var y_2_pmaterial = new THREE.MeshLambertMaterial({ color: 0x000000, transparent: true, opacity: 0 });
    var y_2_pplane = new THREE.Mesh(y_2_pgeometry, y_2_pmaterial);
    y_2_pplane.rotation.y = Math.PI / 2
    y_2_pplane.position.x = -0.5
    this.scene.add(y_2_pplane);
    this.planeObjects.push(y_2_pplane)

    var x_pgeometry = new THREE.PlaneGeometry(1, 1);
    var x_pmaterial = new THREE.MeshLambertMaterial({ color: 0x000000, transparent: true, opacity: 0 });
    var x_pplane = new THREE.Mesh(x_pgeometry, x_pmaterial);
    x_pplane.rotation.x = -Math.PI / 2
    x_pplane.position.y = 0.5
    this.scene.add(x_pplane);
    this.planeObjects.push(x_pplane)

    var x_2_pgeometry = new THREE.PlaneGeometry(1, 1);
    var x_2_pmaterial = new THREE.MeshLambertMaterial({ color: 0x000000, transparent: true, opacity: 0 });
    var x_2_pplane = new THREE.Mesh(x_2_pgeometry, x_2_pmaterial);
    x_2_pplane.rotation.x = Math.PI / 2
    x_2_pplane.position.y = -0.5
    this.scene.add(x_2_pplane);
    this.planeObjects.push(x_2_pplane)


    this.controls = new THREE.OrbitControls(this.camera);
  };

  onResize = ({ width, height, scale }) => {
    this.camera.aspect = width / height;
    this.camera.updateProjectionMatrix();
    this.renderer.setPixelRatio(scale);
    this.renderer.setSize(width, height);
  };

  onRender = delta => {
    // this.cube.rotation.x += 3.5 * delta;
    // this.cube.rotation.y += 2 * delta;
    this.whatFaceIsUp();
    this.renderer.render(this.scene, this.camera);
  };

  whatFaceIsUp = () => {
    this.raycaster.setFromCamera(new THREE.Vector2(0, 0), this.camera);
    const intersects = this.raycaster.intersectObjects(this.planeObjects);
    if (intersects.length > 0) {
      const intersect = intersects[0];
      const selectedIndex = this.planeObjects.indexOf(intersect.object)
      this.setState({ selectedIndex })
      if (this.props.setFaceIndexState)
        this.props.setFaceIndexState(selectedIndex)
    }
  }
}