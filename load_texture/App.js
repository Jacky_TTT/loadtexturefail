import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import RotateDice from './RotateDice'

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <RotateDice
          style={{ flex: 1 }}
          cameraDist={4}
          diceTexture={`123456_F.png`}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
