export default {
  "models": {
    "obj": {
      "dice": {
        "123456_F.png": require('./assets/123456_F.png'),

        "polycube.obj": require('./assets/polycube.obj'),
      },
    },
  },
}